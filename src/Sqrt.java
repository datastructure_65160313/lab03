public class Sqrt {
    static int Sqrt(int x){
        if (x<2) {
            return x;
        }

        int l = 0,r = x;

        while (l<=r) {
            int m = l+(r-l)/2;
            long midSquared = (long) m * m;
            if (midSquared == x) {
                return m;
            } else if (midSquared < x) {
                l = m + 1;
            } else {
                r = m - 1;
            }
        }
        return r;
    }

    public static void main(String[] args) throws Exception {

        int X = 19;
        System.out.println(Sqrt(X));
    }
}
